import React from 'react';
import * as uuid from 'uuid';
import ChatHeader from '../../components/ChatHeader/index';
import MessageList from '../../components/MessageList';
import MessageInput from '../../components/MessageInput';
import Spinner from '../../components/Spinner';
import {getLastMessageByDate, getMemberCount} from '../../helpers/chatHelper';

import styles from './styles.module.css';

class Chat extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            sender: {},
            messages: [],
        }
    }

    componentDidMount() {
        const senderUsername = prompt('Enter your username');
        this.setState({
            sender: {
                user: senderUsername,
                userId: uuid.v4(),
            }
        })

        fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
            .then(res => res.json())
            .then(messages => {
                setTimeout(() => this.setState({
                    messages
                }), 1500);
                setTimeout(() => this.setState({
                    messages: [...this.state.messages,
                        {
                            id: uuid.v4(),
                            userId: uuid.v4(),
                            user: 'Johny',
                            text: 'Testing message animation',
                            createdAt: new Date().toISOString(),
                        }]
                }), 3000);
            })
            .catch(console.error);
    }

    sendMessage = text => {
        const sender = this.state.sender;
        this.setState({
            messages: [...this.state.messages, {
                id: uuid.v4(),
                userId: sender.userId,
                avatar: sender.avatar,
                user: sender.user,
                text,
                createdAt: new Date().toISOString(),
            }]
        })
    };

    editMessage = (id = '') => {
        const messages = this.state.messages;
        const message = messages.find(m => m.id === id);

        message.body = prompt('Edit message', message.text);
        message.editedAt = new Date().toISOString();

        this.setState({messages});
    };

    deleteMessage = (id = '') => {
        const messages = this.state.messages.filter(m => m.id !== id);
        this.setState({messages});
    };

    mapMessage = message => ({
        ...message,
        isOwn: message.user === this.state.sender.user,
        isNew: new Date() - new Date(message.createdAt) < 5e3
    })

    render() {
        const messages = this.state.messages;
        return (
            <div style={{padding: '20px 200px 0', position: 'relative'}}>
                {messages.length ?
                    <>
                        <ChatHeader chatName={'BSA Chat'}
                                    membersCount={getMemberCount(messages)}
                                    messagesCount={messages.length}
                                    lastMessageTime={getLastMessageByDate(messages)}/>
                        <MessageList messages={messages.map(this.mapMessage)}
                                     editMessage={this.editMessage}
                                     deleteMessage={this.deleteMessage}/>
                        <MessageInput sendMessage={this.sendMessage}/>
                    </> : <Spinner/>}

            </div>
        );
    }
}

export default Chat;
