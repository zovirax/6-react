import React from 'react';
import ReactDOM from 'react-dom';
import Chat from './containers/Chat/index';

import './styles/reset.css';
import './styles/common.css';

const root = document.getElementById('root');
ReactDOM.render(
    <React.StrictMode>
    <Chat/>
  </React.StrictMode>,
  root);
