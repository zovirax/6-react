import React from 'react';
import Message from "../Message";
import PropTypes from 'prop-types';

import styles from './styles.module.css';

class MessageList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {deleteMessage, editMessage} = this.props;
        const mapMessage = (message = {}) =>
            <Message
                {...message}
                key={message.id}
                deleteMessage={deleteMessage}
                editMessage={editMessage}
            />;

        return (
            <div className={styles.content}>
                {this.props.messages.map(mapMessage)}
            </div>
        );
    }
}

MessageList.propTypes = {
    messages: PropTypes.arrayOf(PropTypes.any).isRequired,
    deleteMessage: PropTypes.func.isRequired,
    editMessage: PropTypes.func.isRequired,
};

export default MessageList;
